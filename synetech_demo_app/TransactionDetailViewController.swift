//
//  TransactionDetailViewController.swift
//  synetech_demo_app
//
//  Created by David Petrina on 16.12.2021.
//

import UIKit
import UserNotifications

class TransactionDetailViewController: UIViewController {
    
    
    
    @IBOutlet weak var imageCategory: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelCategory: UILabel!
    @IBOutlet weak var labelAmount: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelAmountPlaceholder: UILabel!
    @IBOutlet weak var labelDatePlaceholder: UILabel!
    
    var titleData = String()
    var categoryData = String()
    var amountData = String()
    var dateData = String()
    var categoryImage = UIImage()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        view.layer.contents = #imageLiteral(resourceName: "Rectangle").cgImage
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationItem.title = "Detail"
        
        let navBarAppearance = UINavigationBarAppearance()
        navBarAppearance.backgroundColor = .white
        navigationController?.navigationBar.standardAppearance = navBarAppearance
        navigationController?.navigationBar.scrollEdgeAppearance = navBarAppearance
        
        let favBtn = UIBarButtonItem(image: UIImage(systemName: "star"), style: .done, target: self, action: #selector(sendNotification))
        navigationItem.setRightBarButton(favBtn, animated: true)
        
        imageCategory.image = categoryImage
        labelTitle.text = titleData
        labelTitle.textColor = .white
        labelCategory.text = categoryData
        labelCategory.textColor = .white
        labelAmount.text = amountData
        labelAmount.textColor = .white
        labelAmount.textAlignment = .right
        //        labelDate.text = dateData
        labelDate.textColor = .white
        labelDate.textAlignment = .right
        labelDatePlaceholder.textColor = .white
        labelAmountPlaceholder.textColor = .white
        labelDate.text = dateData
        
        
        let lineAboveTitle = labelTitle.frame.origin.y + labelTitle.font.ascender - 20
        let lineBelowCategory = labelCategory.frame.origin.y + 40
        let lineBelowAmount = labelAmount.frame.origin.y + 35
        let lineBelowDate = labelDate.frame.origin.y + 40
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 0, y: lineAboveTitle))
        path.addLine(to: CGPoint(x: view.bounds.maxX, y: lineAboveTitle))
        path.move(to: CGPoint(x:0, y: lineBelowCategory))
        path.addLine(to: CGPoint(x: view.bounds.maxX, y: lineBelowCategory))
        path.move(to: CGPoint(x: labelAmountPlaceholder.frame.origin.x, y: lineBelowAmount))
        path.addLine(to: CGPoint(x: labelAmount.frame.maxX, y: lineBelowAmount))
        path.move(to: CGPoint(x: 0, y: lineBelowDate))
        path.addLine(to: CGPoint(x: view.bounds.maxX, y: lineBelowDate))


        let shapeLayer = CAShapeLayer()
        shapeLayer.path = path.cgPath
        shapeLayer.strokeColor = UIColor.gray.cgColor
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.lineWidth = 1
    

        view.layer.addSublayer(shapeLayer)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    @objc func sendNotification() {
        let center = UNUserNotificationCenter.current()
        
        let notification = UNMutableNotificationContent()
        
        notification.body = "You narcistic piece of shit."
        notification.sound = UNNotificationSound.default
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 0.5, repeats: false)
        let request = UNNotificationRequest(identifier: "detailNotification", content: notification, trigger: trigger)
        
        center.add(request) { (error: Error?) in
            if let error = error {
                print(error.localizedDescription)
            }
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
