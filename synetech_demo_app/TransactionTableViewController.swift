//
//  TransactionTableViewController.swift
//  synetech_demo_app
//
//  Created by David Petrina on 16.12.2021.
//

import UIKit

class TransactionTableViewController: UITableViewController {
    
    
    @IBOutlet var transactionTableView: UITableView!
    
    var apiCall = API()
    var customersData = Customer()
    var transactionsData = CustomerDetail(customerID: 0, customerCode: "", customerName: "", transactions: [])
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        loadData()
        transactionTableView.reloadData()
        transactionTableView.delegate = self
        transactionTableView.dataSource = self
        transactionTableView.backgroundView = UIImageView(image: UIImage(named: "Rectangle"))
        transactionTableView.separatorColor = .white
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(true, animated: true)
        navigationItem.title = "Transactions"
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return transactionsData.transactions.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "transactionCell", for: indexPath) as! TransactionTableViewCell
        
        cell.imageCategory.image = transactionsData.transactions[indexPath.row].type.icon
//        cell.imageCategory.image = UIImage(named: "\(transactionsData.transactions[indexPath.row].type.rawValue)"+"_icon")
        cell.labelTitle.text = transactionsData.transactions[indexPath.row].transactionDescription
        cell.labelTitle.textColor = .white
        cell.labelCategory.text = transactionsData.transactions[indexPath.row].type.category
        cell.labelCategory.textColor = .gray
        
        let amountFormatter = NumberFormatter()
        amountFormatter.numberStyle = .currency
        amountFormatter.maximumFractionDigits = 2
        cell.labelAmount.text = amountFormatter.string(from: NSNumber(value: transactionsData.transactions[indexPath.row].amount))
//        cell.labelAmount.text = "\(String(transactionsData.transactions[indexPath.row].amount))"+" Є"
        cell.labelAmount.textAlignment = .right
        cell.labelAmount.textColor = .white
        cell.imageArrow.image = UIImage(systemName: "chevron.right")
        cell.imageArrow.tintColor = .white
        cell.backgroundColor = .clear
        cell.separatorInset = .zero
        
        
        return cell
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
         
         if segue.identifier == "detailSegue", let vc = segue.destination as? TransactionDetailViewController, let indexPath = transactionTableView.indexPathForSelectedRow {
             let amountFormatter = NumberFormatter()
             amountFormatter.numberStyle = .currency
             amountFormatter.maximumFractionDigits = 2
             
             vc.titleData = transactionsData.transactions[indexPath.row].transactionDescription
             vc.categoryData = transactionsData.transactions[indexPath.row].type.category
             vc.amountData = amountFormatter.string(from: NSNumber(value: transactionsData.transactions[indexPath.row].amount))!
             let dateString = transactionsData.transactions[indexPath.row].date
             let dateFormatter = DateFormatter()
             dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
             dateFormatter.locale = Locale.current
             guard let newDate = dateFormatter.date(from: dateString) else {return}
             
             let newDateFormatter = DateFormatter()
             newDateFormatter.dateFormat = "dd.MM.yyyy"
             let result = newDateFormatter.string(from: newDate)
             vc.dateData = result
             vc.categoryImage = transactionsData.transactions[indexPath.row].type.icon!
         }
     }
    
    func loadData() {
        apiCall.fetchDataCustomers() { (customers) in
            DispatchQueue.main.async {
                self.customersData = customers
                self.transactionTableView.reloadData()
                if !self.customersData.isEmpty {
                    let custIdElement = self.customersData.filter { $0.customerName == "SYNETECH s.r.o." }
                    let custId = custIdElement.map({ (customer: CustomerElement) -> Int in
                        customer.customerID
                    })
                    self.apiCall.fetchDataTransactions(customerId: custId[0]) { (transactions) in
                        DispatchQueue.main.async {
                            self.transactionsData = transactions
                            self.transactionTableView.reloadData()
                        }
                    }
                }
            }
        }
    }
}
