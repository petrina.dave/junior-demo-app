//
//  API.swift
//  synetech_demo_app
//
//  Created by David Petrina on 16.12.2021.
//

import Foundation

class API {
    
    func fetchDataCustomers(_ completion: @escaping(Customer) -> Void) {
        let url = URL(string: "https://stub.bbeight.synetech.cz/v1/customers")!
        let task = URLSession.shared.dataTask(with: url, completionHandler: { data, response, error in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            if let data = data,
               let customersData = try? JSONDecoder().decode(Customer.self, from: data) {
                completion(customersData)
            }
        })
        task.resume()
    }
    
    func fetchDataTransactions(customerId: Int, _ completion: @escaping(CustomerDetail) -> Void) {
        let url = URL(string: "https://stub.bbeight.synetech.cz/v1/customers/\(customerId)")!
        let task = URLSession.shared.dataTask(with: url, completionHandler: { data, response, error in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            if let data = data,
               let transactionsData = try? JSONDecoder().decode(CustomerDetail.self, from: data) {
                completion(transactionsData)
            }
        })
        task.resume()
    }
}
