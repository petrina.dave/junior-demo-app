//
//  Customer.swift
//  synetech_demo_app
//
//  Created by David Petrina on 16.12.2021.
//

import Foundation

// MARK: - CustomerElement
struct CustomerElement: Codable {
    let customerID: Int
    let customerName: String
    let customerDesc: String?

    enum CodingKeys: String, CodingKey {
        case customerID = "customerId"
        case customerName, customerDesc
    }
}

typealias Customer = [CustomerElement]
