//
//  CustomerDetail.swift
//  synetech_demo_app
//
//  Created by David Petrina on 16.12.2021.
//

import Foundation
import UIKit

// MARK: - Customer
struct CustomerDetail: Codable {
    let customerID: Int
    let customerCode, customerName: String
    let transactions: [Transaction]

    enum CodingKeys: String, CodingKey {
        case customerID = "customerId"
        case customerCode, customerName, transactions
    }
}

// MARK: - Transaction
struct Transaction: Codable {
    let id: Int
    let type: TypeEnum
    let transactionDescription, date: String
    let amount: Double

    enum CodingKeys: String, CodingKey {
        case id, type
        case transactionDescription = "description"
        case date, amount
    }
}

enum TypeEnum: String, Codable {
    case bank = "bank"
    case gas = "gas"
    case house = "house"
    case kids = "kids"

    var icon: UIImage? {
        if case .house = self {
            return UIImage(named: "home_icon")
        }
        return UIImage(named: rawValue + "_icon")
    }
    
    var category : String {
        return rawValue.capitalized
    }
}
